function campo(clase)
{
    var input=document.createElement("input");
    input.setAttribute("class","form-control");
    input.type=clase;
    document.body.appendChild(input);
    
    return input;
}

function tipos(et1,et2)
{
    var tipo=document.createElement(et1);
    var texto=document.createTextNode(et2);
    
    tipo.appendChild(texto);
    
    return tipo;
}

function password(tipo,placeholder)
{
     var input=document.createElement("input");
     input.setAttribute("class","form-control");
     input.type=tipo;
     document.body.appendChild(input);
     
     return input;
     
     
}

function leyen(tipo,clase)
{
     var p=document.createElement("p");
     p.setAttribute("class","help_block");
     document.body.appendChild(p);
     
     return p;
     
     
}
function textarea(){
  var textarea=document.createElement("textarea");
  textarea.setAttribute("class","form-control");
  document.body.appendChild(textarea);
  return textarea;
}

function checkbox(tipo){
  var checkbox=document.createElement("input");
  checkbox.setAttribute("class","form-control");
  checkbox.type=tipo;
  document.body.appendChild(checkbox);
  return checkbox;  
}

function cuadrocolor(color){
   var colorx=document.createElement("input");
   colorx.setAttribute("class","form-control");
   colorx.type="color";
   colorx.value=color;
   document.body.appendChild(colorx);
   return colorx;   
}

function barraprogreso(tipo){
  var barra=document.createElement("progress");
   barra.setAttribute("class","form-control");
  barra.type=tipo;
  document.body.appendChild(barra);
  return barra;   
}

function input_numero(tipo,min,max,step){
  var numero=document.createElement("input");
  numero.setAttribute("class","form-control");
  document.body.appendChild(numero);
  numero.type=tipo;
  numero.min=min;
  numero.max=max;
  numero.step=step;
  return numero;
}

function lista_numeros()
{
    var lista= document.createElement("select");
    var grupo= document.createElement("optgroup");
    grupo.label="elementos";
    var opcion1=document.createElement("option");
    var opcion2=document.createElement("option");
    var opcion3=document.createElement("option");
    var texto1=document.createTextNode("elemento1");
    var texto2=document.createTextNode("elemento2");
    var texto3=document.createTextNode("elemento3");
    opcion1.appendChild(texto1);
    opcion2.appendChild(texto2);
    opcion3.appendChild(texto3);
    grupo.appendChild(opcion1);
    grupo.appendChild(opcion2);
    grupo.appendChild(opcion3);
    lista.appendChild(grupo);
    document.body.appendChild(lista);
}

function seleccion(tipo,valor){
 var input=document.createElement('input');
 input.setAttribute("class","form-control");
 input.type=tipo;
 input.checked=valor;
 document.body.appendChild(input);
 return input;
}

function leyenda()
{
    var fieldset=document.createElement("fieldset");
    var leyenda=document.createElement("legend");
    var label= document.createElement("label");
    var input= document.createElement("input");
    var texto1= document.createTextNode("registro");
    var texto2=document.createTextNode("nombre");
    label.for="field";
    input.type="text";
    leyenda.appendChild(texto1);
    label.appendChild(texto2);
    fieldset.appendChild(leyenda);
    fieldset.appendChild(label)
    fieldset.appendChild(input);
    document.body.appendChild(fieldset);
    
}

function forma()
{
    var forma=document.createElement("form");
    var div=document.createElement("div");
    
    forma.setAttribute("role","form");
    div.setAttribute("class","col-md-4");
    
    var tiptitulo=tipos("h2","Formulario Basico");
    var tipoetiqueta1=tipos("label","Email");
    var campo1=campo("email");
    var tipoetiqueta2=tipos("label","contraseña");
    var campo2=campo("password","password");
    var tipoetiqueta3=tipos("label","Adjuntar un archivo")
    var adjunto=document.createElement("input");
    adjunto.type="file";
    var p=document.createElement("p");
    p.setAttribute("class","help_block");
    var texto=document.createTextNode("ejemplo de texto ayuda");
    p.appendChild(texto);
    var tipoetiqueta4=tipos("label","Adjuntar un archivo");
    var p2=document.createElement("p");
    p2.setAttribute("class","help_block");
    
    div.appendChild(tiptitulo);
    div.appendChild(tipoetiqueta1);
    div.appendChild(campo1);
    div.appendChild(tipoetiqueta2);
    div.appendChild(campo2);
    div.appendChild(tipoetiqueta3);
    div.appendChild(adjunto);
    div.appendChild(p);
    div.appendChild(p2);

    forma.appendChild(div);
    
    document.body.appendChild(forma);
    return forma;
    
}
function forma2()
{
    var forma=document.createElement("form");
    var div=document.createElement("div");
    
    forma.setAttribute("role","form");
    div.setAttribute("class","col-md-8");
    
    var selc=seleccion("radio","true");
    var color=cuadrocolor("#66FFCC");
    var area= textarea();
    
    div.appendChild(selc);
    div.appendChild(color);
    div.appendChild(area);
    
     forma.appendChild(div);
    
    document.body.appendChild(forma);
    return forma;

}

function forma2()
{
    var forma=document.createElement("form");
    var div=document.createElement("div");
    
    forma.setAttribute("role","form");
    div.setAttribute("class","col-md-6");
    
	var slider=campo("range");
	var area= textarea();
	div.appendChild(campo);
    div.appendChild(area);
	
	document.body.appendChild(forma);
    return forma;
}


function forma3()
{
    var forma=document.createElement("form");
    var div=document.createElement("div");
    
    forma.setAttribute("role","form");
    div.setAttribute("class","col-md-1");
    
	var slider=campo("range");
	var area= textarea();
	div.appendChild(campo);
    div.appendChild(area);
	
	document.body.appendChild(forma);
    return forma;
}



forma();